package net.tncy.lly.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void isValidCode()
    {
        ISBNValidator v = new ISBNValidator();
        String isbn = "0-1100-3523-2";
        assertTrue( v.isValid(isbn, null) );
    }
    
    @Test
    public void isInvalidCode()
    {
        ISBNValidator v = new ISBNValidator();
        String isbn = "0-11";
        assertFalse( v.isValid(isbn, null) );
    }
    
    @Test
    public void isInvalidCharacterCode()
    {
        ISBNValidator v = new ISBNValidator();
        String isbn = "0-1&&0-3523-2";
        assertFalse( v.isValid(isbn, null) );
    }
    
    @Test
    public void isEmptyCodeInvalidCode()
    {
        ISBNValidator v = new ISBNValidator();
        String isbn = "";
        assertFalse( v.isValid(isbn, null) );
    }
}
